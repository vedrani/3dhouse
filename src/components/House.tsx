import useStyles from "./useStyles";
import { Direction, Range } from "react-range";

const roofAngles = [0, 10, 25, 35, 45, 90];

type HouseProps = {
  roofAngle: number;
  roofOrientation: number;
  onRoofAngleChange: (roofAngle: number) => void;
  onRoofOrientationChange: (roofOrientation: number) => void;
};

function House({
  roofAngle,
  roofOrientation,
  onRoofAngleChange,
  onRoofOrientationChange,
}: HouseProps) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.modelContainer}>
        <div className={classes.compas}>
          <img alt="compass" src="/img/compass.svg" />
          <div className={classes.compasValue}>{`${roofOrientation}°`}</div>
          <div
            className={classes.compasPointer}
            style={{ transform: `rotateZ(${roofOrientation}deg)` }}
          >
            <img
              alt="compass-pointer"
              className={classes.compasPointerIcon}
              src="/img/compass-pointer.svg"
            />
          </div>
        </div>
        <div className={classes.houseModel}>
          <img
            alt="house"
            className={classes.houseModelImage}
            src={`img/models/${roofAngle}/${
              (270 - roofOrientation) / 3 + 1
            }.jpg`}
          />
        </div>
      </div>
      <h4 className={classes.rangeHeadline}>Sklon střechy</h4>
      <Range
        step={1}
        min={0}
        max={5}
        values={[roofAngles.findIndex((a) => a === roofAngle)]}
        onChange={([value]) => onRoofAngleChange(roofAngles[value])}
        renderTrack={({ props, children }) => (
          <div>
            <div
              className={classes.rangeSliderTrack}
              {...props}
              style={{ ...props.style }}
            >
              {children}
            </div>
          </div>
        )}
        renderThumb={({ props }) => (
          <div
            className={classes.rangeSliderThumb}
            {...props}
            style={{ ...props.style }}
          >
            <div className={classes.rangeSliderThumbLabel}>{roofAngle}°</div>
          </div>
        )}
        renderMark={({ props, index }) => (
          <div
            className={classes.rangeSliderMark}
            {...props}
            style={{ ...props.style }}
          >
            <div className={classes.rangeSliderMarkLabel}>{roofAngle}°</div>
          </div>
        )}
      />
      <h4 className={classes.rangeHeadline}>Orientace střechy</h4>
      <Range
        step={3}
        min={90}
        max={270}
        direction={Direction.Left}
        values={[roofOrientation]}
        onChange={([value]) => onRoofOrientationChange(value)}
        renderTrack={({ props, children }) => (
          <div>
            <div
              className={classes.rangeSliderTrack}
              {...props}
              style={{ ...props.style }}
            >
              {children}
            </div>
          </div>
        )}
        renderThumb={({ props }) => (
          <div
            className={classes.rangeSliderThumb}
            {...props}
            style={{ ...props.style }}
          >
            <div className={classes.rangeSliderThumbLabel}>
              {`${roofOrientation}° ${
                (roofOrientation === 270 ? "Z" : "") ||
                (roofOrientation < 270 && roofOrientation > 180 ? "JZ" : "") ||
                (roofOrientation === 180 ? "J" : "") ||
                (roofOrientation < 180 && roofOrientation > 90 ? "JV" : "") ||
                (roofOrientation === 90 ? "V" : "")
              }`}
            </div>
          </div>
        )}
        renderMark={({ props, index }) =>
          (index === 0 ||
            index === 15 ||
            index === 30 ||
            index === 45 ||
            index === 60) && (
            <div
              className={classes.rangeSliderMark}
              {...props}
              style={{ ...props.style }}
            >
              <div className={classes.rangeSliderMarkLabel}>
                {index === 0 ? "270° západ" : ""}
                {index === 15 ? "225°" : ""}
                {index === 30 ? "180° jih" : ""}
                {index === 45 ? "135°" : ""}
                {index === 60 ? "90° východ" : ""}
              </div>
            </div>
          )
        }
      />
    </div>
  );
}

export default House;
