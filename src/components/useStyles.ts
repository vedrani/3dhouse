import { Theme, makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: "20px auto",
      maxWidth: "730px",
    },
    rangeHeadline: {
      fontSize: "16px",
      textAlign: "left",
      marginBottom: "20px",
      fontFamily: "RoobertCEZ,Arial,sans-serif",
      textTransform: "uppercase",
      position: "relative",
    },
    modelContainer: {
      position: "relative",
      overflow: "hidden",
      paddingTop: "100px",
      [theme.breakpoints.up("sm")]: {
        overflow: "visible",
      },
      [theme.breakpoints.up("md")]: {
        paddingTop: "60px",
      },
      [theme.breakpoints.up("lg")]: {
        marginLeft: "-96px",
        marginRight: "-96px",
        paddingTop: 0,
        marginBottom: "-40px",
      },
    },
    compas: {
      position: "absolute",
      top: 0,
      right: "24px",
      width: "112px",
      height: "112px",
      zIndex: 1,
      [theme.breakpoints.up("lg")]: {
        right: "56px",
      },
    },
    compasPointer: {
      position: "absolute",
      width: "16px",
      height: "88px",
      left: "50%",
      top: "50%",
      marginLeft: "-8px",
      marginTop: "-44px",
      transition: "transform 50ms ease-out",
    },
    compasPointerIcon: {
      position: "absolute",
      width: "16px",
      height: "18px",
      top: 0,
      left: 0,
      fill: "#f24f00",
    },
    compasValue: {
      position: "absolute",
      width: "100%",
      top: "50%",
      left: 0,
      textAlign: "center",
      fontSize: "14px",
      transform: "translateY(-50%)",
    },
    houseModel: {
      height: "255px",
      position: "relative",
      transform: "scale(1.3) translateX(-8px)",
      [theme.breakpoints.up("sm")]: {
        paddingTop: 0,
        height: "255px",
        transform: "none",
      },
      [theme.breakpoints.up("md")]: {
        height: "345px",
      },
      [theme.breakpoints.up("lg")]: {
        height: "366px",
      },
    },
    houseModelImage: {
      position: "absolute",
      width: "100%",
      left: 0,
      top: 0,
    },
    rangeSliderTrack: {
      height: "1px",
      width: "100%",
      backgroundColor: "#e6e6e6",
      marginBottom: "76px",
    },
    rangeSliderThumb: {
      height: "8px",
      width: "8px",
      background: "linear-gradient(90.89deg, #fa6900 0%, #f24f00 100%)",
      transition: "transform 100ms ease-out",
      borderRadius: "50%",
      outline: "none",
    },
    rangeSliderThumbLabel: {
      position: "absolute",
      left: "50%",
      top: "24px",
      fontFamily: "RoobertCEZ, sans-serif",
      textTransform: "uppercase",
      fontWeight: "bold",
      color: "#ffffff",
      background: "linear-gradient(90.89deg, #fa6900 0%, #f24f00 100%)",
      boxShadow: "0 4px 8px rgba(26, 26, 26, 0.2)",
      padding: "8px 13px 8px 20px",
      borderRadius: "8px",
      transform: "translateX(-50%)",
      whiteSpace: "nowrap",
      "&::before": {
        position: "absolute",
        content: "''",
        width: "24px",
        height: "12px",
        top: "-12px",
        left: "50%",
        transform: "translateX(-50%)",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center center",
        backgroundImage:
          'url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyNS4yLjEsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDIxLjIgMTEuOSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMjEuMiAxMS45OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDp1cmwoI1NWR0lEXzFfKTt9DQo8L3N0eWxlPg0KPGcgaWQ9IlZyc3R2YV8xIj4NCjwvZz4NCjxnIGlkPSJWcnN0dmFfMiI+DQoJPGxpbmVhckdyYWRpZW50IGlkPSJTVkdJRF8xXyIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIHgxPSItMS42Nzc5ODFlLTAyIiB5MT0iNS44NjA0IiB4Mj0iMjEuMzQzNSIgeTI9IjYuMjQyOCI+DQoJCTxzdG9wICBvZmZzZXQ9IjAiIHN0eWxlPSJzdG9wLWNvbG9yOiNGQTY5MDAiLz4NCgkJPHN0b3AgIG9mZnNldD0iMSIgc3R5bGU9InN0b3AtY29sb3I6I0YyNEYwMCIvPg0KCTwvbGluZWFyR3JhZGllbnQ+DQoJPHBhdGggY2xhc3M9InN0MCIgZD0iTTIxLjIsMTEuOWMtNi0wLjctMTAuNi01LjgtMTAuNi0xMS45QzEwLjYsNi4yLDYsMTEuMiwwLDExLjlIMjEuMnoiLz4NCjwvZz4NCjwvc3ZnPg0K")',
        backgroundSize: "100%",
      },
    },
    rangeSliderMark: {
      width: "9px",
      height: "9px",
      backgroundColor: "#e6e6e6",
      border: "2px solid #ffffff",
      borderRadius: "50%",
      marginTop: "-4px !important",
    },
    rangeSliderMarkLabel: {
      position: "absolute",
      top: "22px",
      left: "50%",
      transform: "translateX(-50%)",
      color: "#959595",
      fontSize: "14px",
      lineHeight: "1.4",
      whiteSpace: "nowrap",
    },
  })
);

export default useStyles;
