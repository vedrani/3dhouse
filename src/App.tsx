import { useState } from "react";
import House from "./components/House";

function App() {
  const [state, setState] = useState({
    roofAngle: 0,
    roofOrientation: 270,
  });

  return (
    <House
      roofAngle={state.roofAngle}
      roofOrientation={state.roofOrientation}
      onRoofAngleChange={(roofAngle) => setState({ ...state, roofAngle })}
      onRoofOrientationChange={(roofOrientation) =>
        setState({ ...state, roofOrientation })
      }
    />
  );
}

export default App;
